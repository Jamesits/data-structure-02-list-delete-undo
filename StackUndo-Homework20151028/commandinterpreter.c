#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "common.h"
#include "commandinterpreter.h"
#include "datalist.h"
#include "libio.h"

typedef struct {
	char *command;
	int(*eval)(char *s);
	char *help;
} cmd_dict;

cmd_dict commands[] = {
	{ "ADD",	add,		"Add an entry to the list." },
	{ "CLEAR",	cls,		"" },
	{ "CLS",	cls,		"Clears the screen." },
	{ "COUNT",	count,		"Display entry number of the list."},
	{ "DEL",	del,		"Remove an entry from the list." },
	{ "DIR",	list,		"" },
	{ "EXIT",	quit,		"Quits this software (command interpreter)." },
	{ "H",		help,		"" },
	{ "HELP",	help,		"Provides Help information for commands available." },
	{ "LIST",	list,		"Display all entries in the list." },
	{ "LS", 	list,		"" },
	{ "LOAD",	load,		"Load list from a text file." },
	{ "MAN",	help,		"" },
	{ "RM",		del,		"" },
	{ "SAVE",	save,		"Save current list to a text file." },
	{ "TOUCH",	add,		"" },
	{ "QUIT",	quit,		"" },
	{ "REDO",	lredo,		"Redo the last command undone." },
	{ "UNAME",	version,	"" },
	{ "UNDO",	lundo,		"Undo the last command." },
	{ "VER",	version,	"Displays the software version." },
	{ "VERSION",version,	"" },
};

static student_list *l;
static bool mute = false;

bool is_arg_present(char *arg) {
	if (arg == NULL) return false;
	while (*arg != 0 && isspace(*arg)) arg++;
	if (*arg == 0) return false;
	return true;
}

int help(char *arg) {
	for (int i = 0; i<sizeof commands / sizeof commands[0]; ++i) {
		if (strlen(commands[i].help)) printf("%s\t\t%s\n", commands[i].command, commands[i].help);
	}
	return EXIT_SUCCESS;
}

int version(char *arg) {
	puts("undo-example [Version " VERSION "]\n(c)2015 James Swineson. All rigits reserved.\nPowered by libJInterpreter.\n");
	return EXIT_SUCCESS;
}

int cls(char *arg) {
	system("@cls||clear");
	puts("");
	return EXIT_SUCCESS;
}

int quit(char *arg) {
	puts("Quitting...");
	return EXIT_FAILURE;
}

int add(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Nothing to add.\n");
		return EXIT_SUCCESS;
	}
	add_student(l, arg);
	if (!mute) printf("New entry #%u.\n", l->count);
	return EXIT_SUCCESS;
}

int del(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Nothing to delete.\n");
		return EXIT_SUCCESS;
	}
	unsigned id;
	if (!sscanf(arg, "%u", &id) || id >= l->count) {
		fprintf(stderr, "Error: Unknown ID #%u.\n", id);
		return EXIT_SUCCESS;
	}
	delete_student(l, id);
	if (!mute) printf("Entry #%u removed.\n", id);
	return EXIT_SUCCESS;
}

int count(char *arg) {
	reset_list_iterator(l);
	student *s;
	unsigned count = 0;
	while (s = list_iterator_move_next(l), s != NULL) {
		count++;
	}
	printf("%u\n", count);
	reset_list_iterator(l);
	return EXIT_SUCCESS;
}

int list(char *arg) {
	walk_student_list(l);
	return EXIT_SUCCESS;
}

int lundo(char *arg) {
	unsigned times = 1;
	if (!is_arg_present(arg)) {
		times = !undo_student_list(l);
	}
	else if (sscanf(arg, "%u", &times)) {
		times = undo_student_list_times(l, times);
	}
	else {
		fprintf(stderr, "Error: Unknown arguments.\n");
		return EXIT_SUCCESS;
	}
	printf("Performed %u times of undo.\n", times);
	return EXIT_SUCCESS;
}

int lredo(char *arg) {
	unsigned times = 1;
	if (!is_arg_present(arg)) {
		times = !redo_student_list(l);
	}
	else if (sscanf(arg, "%u", &times)) {
		times = redo_student_list_times(l, times);
	}
	else {
		fprintf(stderr, "Error: Unknown arguments.\n");
		return EXIT_SUCCESS;
	}
	printf("Performed %u times of redo.\n", times);
	return EXIT_SUCCESS;
}

int save(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[FILENAME_MAX_LENGTH + 1];
	if (sscanf(arg, "%"FILENAME_MAX_LENGTH_CHAR"s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}

	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}

	FILE *dest = fopen(arg, "w");
	if (dest == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else {
		reset_list_iterator(l);
		student *s;
		unsigned count = 0;
		while (s = list_iterator_move_next(l), s != NULL) {
			fprintf(dest, "%s\n", &(s->name));
			count++;
		}
		fclose(dest);
		printf("%u record(s) exported.\n", count);
		reset_list_iterator(l);
		return EXIT_SUCCESS;
	}
}

int load(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[FILENAME_MAX_LENGTH + 1];
	if (sscanf(arg, "%"FILENAME_MAX_LENGTH_CHAR"s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}

	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}

	FILE *src = fopen(arg, "r");
	if (src == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else {
		mute = true;
		unsigned count = 0;
		char *c;
		int result;
		while (result = freadline(src, &c), result != EXIT_FAILURE && result != EOF) {
			add_student(l, c);
			free(c);
			count++;
		}
		fclose(src);
		mute = false;
		printf("%u record(s) imported.\n", count);
		return EXIT_SUCCESS;
	}
}

int runcommand(char *s)
{
	char temp[20];
	if (sscanf(s, "%20s", temp) != EOF) {
		temp[19] = 0;
		for (int i = 0; i < strlen(temp); ++i) temp[i] = toupper(temp[i]);
		for (int i = 0; i < sizeof commands / sizeof commands[0]; ++i) {
			if (strcmp(temp, commands[i].command) == 0) {
				char *t = s + strlen(commands[i].command);
				if (isspace(*t)) t++;
				if ( *t == 0 ) t = NULL;
				return (*(commands[i].eval))(t);
			}
		}
		sscanf(s, "%20s", temp);
		printf("'%s' is not recognized as an internal command.\n", temp);
	}
	return EXIT_SUCCESS;
}

int loop()
{
	l = create_student_list();
	char cmd[COMMAND_MAX_LENGTH + 1] = { 0 };
	do {
		printf(PROMPT);
	} while (gets_s(cmd, COMMAND_MAX_LENGTH)!= NULL && !runcommand(cmd));
	return EXIT_SUCCESS;
}