#define _CRT_SECURE_NO_WARNINGS
#include "datalist.h"
#include "undostack.h"

student_list *create_student_list() {
	student_list *l = malloc(sizeof(student_list));
	l->list = malloc(LIST_ALLOCATE_STEP * sizeof(student*));
	l->len = LIST_ALLOCATE_STEP;
	l->count = 0;
	return l;
}

void purge_student_list(student_list *l) {
	while (l->len) free(l->list[--(l->len)]);
	free(l->list);
}

void walk_student_list(student_list *l) {
	puts("Seq.\tID\tName");
	for (unsigned i = 0, count = 0; i < l->count; i++)
		if (!(l->list[i]->isDeleted)) printf("%u\t%u\t%s\n", ++count, l->list[i]->id, &(l->list[i]->name));
	puts("");
}

unsigned append_student(student_list *l, student *s) {
	while (l->count >= l->len) {
		l->list = realloc(l->list, (l->len += LIST_ALLOCATE_STEP) * sizeof(student*));
		if (l->list == NULL) { fprintf(stderr, "Memory error!"); exit(1); }
	}
	l->list[l->count] = s;
	l->count++;
	s->id = l->count - 1;
	return s->id;
}

int add_student(student_list *list, char *name) {
	student *s = malloc(sizeof(student) + (strlen(name) + 1) * sizeof(char));
	strcpy(&(s->name), name);
	s->isDeleted = false;
	unsigned id = append_student(list, s);
	did(OPERATION_ADD, id);
	return id;
}

void delete_student(student_list *list, unsigned id) {
	if (id < list->count) list->list[id]->isDeleted = true;
	did(OPERATION_DELETE, id);
}

int undo_student_list(student_list *list) {
	operation *op = undo();
	if (op->id < list->count) {
		switch (op->type) {
		case OPERATION_ADD: list->list[op->id]->isDeleted = true; return EXIT_SUCCESS; break;
		case OPERATION_DELETE: list->list[op->id]->isDeleted = false; return EXIT_SUCCESS; break;
		}
	}
	return EXIT_FAILURE;
}

unsigned undo_student_list_times(student_list *list, unsigned times) {
	unsigned count = 0;
	while (count++, times-- > 0) if (undo_student_list(list)) return --count;
	return --count;
}

int redo_student_list(student_list *list) {
	operation *op = redo();
	if (op->id < list->count) {
		switch (op->type) {
		case OPERATION_ADD: list->list[op->id]->isDeleted = false; return EXIT_SUCCESS; break;
		case OPERATION_DELETE: list->list[op->id]->isDeleted = true; return EXIT_SUCCESS; break;
		}
	}
	return EXIT_FAILURE;
}

unsigned redo_student_list_times(student_list *list, unsigned times) {
	unsigned count = 0;
	while (count++, times-- > 0) if (redo_student_list(list)) return --count;
	return --count;
}

void reset_list_iterator(student_list *list) {
	list->__iterator_pos = 0;
}

student *list_iterator_move_next(student_list *list) {
	while (list->__iterator_pos < list->count && list->list[list->__iterator_pos]->isDeleted == true) list->__iterator_pos++;
	if (list->__iterator_pos >= list->count) return NULL;
	return list->list[list->__iterator_pos++];
}

