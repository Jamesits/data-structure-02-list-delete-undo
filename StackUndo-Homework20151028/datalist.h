#pragma once
#ifndef __DATALIST_H__
#define __DATALIST_H__

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIST_ALLOCATE_STEP 16

typedef struct {
	unsigned id;
	bool isDeleted;
	char name;
} student;

typedef struct {
	unsigned int len;
	unsigned int count;
	unsigned int __iterator_pos;
	student** list;
} student_list;

student_list *create_student_list();
int add_student(student_list *list, char *name);
void purge_student_list(student_list *list);
void walk_student_list(student_list *l);
void delete_student(student_list *list, unsigned id);
int undo_student_list(student_list *list);
unsigned undo_student_list_times(student_list *list, unsigned times);
int redo_student_list(student_list *list);
unsigned redo_student_list_times(student_list *list, unsigned times);
void reset_list_iterator(student_list *list);
student *list_iterator_move_next(student_list *list);

#endif