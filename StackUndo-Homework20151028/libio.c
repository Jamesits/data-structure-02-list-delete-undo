#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libio.h"

int freadline(FILE *file, char **result) {

	if (file == NULL) {
		fprintf(stderr, "Error: file pointer is null.");
		return EXIT_FAILURE;
	}

	const unsigned buffer_allocate_step = 16;
	unsigned buffer_size = buffer_allocate_step;
	char *lineBuffer = (char *)malloc(sizeof(char) * buffer_allocate_step);

	if (lineBuffer == NULL) {
		fprintf(stderr, "Error allocating memory for line buffer.");
		return EXIT_FAILURE;
	}

	char ch = getc(file);
	unsigned count = 0;

	while ((ch != '\n') && (ch != EOF)) {
		if (count >= buffer_size) {
			buffer_size += buffer_allocate_step;
			lineBuffer = realloc(lineBuffer, buffer_size);
			if (lineBuffer == NULL) {
				fprintf(stderr, "Error reallocating space for line buffer.");
				return EXIT_FAILURE;
			}
		}
		lineBuffer[count] = ch;
		count++;

		ch = getc(file);
	}
	lineBuffer[count] = '\0';
	lineBuffer = realloc(lineBuffer, count + 1);
	*result = lineBuffer;

	if (ch == EOF) return EOF;
	
	return EXIT_SUCCESS;
}

int fwriteline(FILE *file, char *output) {
	return fprintf(file, "%s\n", output);
}