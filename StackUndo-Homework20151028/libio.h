#pragma once
#ifndef __LIBIO_H__
#define __LIBIO_H__

int freadline(FILE *file, char **result);
int fwriteline(FILE *file, char *output);

#endif