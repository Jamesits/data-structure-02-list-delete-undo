#include "undostack.h"

static operation **opstack = NULL;
static int stack_size = 0, stack_count = 0;

int stack_append(operation *op) {
	if (opstack == NULL) {
		opstack = malloc(LIST_ALLOCATE_STEP * sizeof(operation*));
		stack_size = LIST_ALLOCATE_STEP;
	}
	if (stack_count >= stack_size - 1) {
		opstack = realloc(opstack, (stack_size + LIST_ALLOCATE_STEP) * sizeof(operation*));
		stack_size += LIST_ALLOCATE_STEP;
	}
	opstack[stack_count] = op;
	for (int i = stack_count + 1; i < stack_size; i++) opstack[i] = NULL;
	return ++stack_count;
}

operation *create_empty_op() {
	static operation *op = NULL;
	if (op == NULL) op = malloc(sizeof(operation));
	op->type = OPEREATION_NULL;
	op->id = 0;
	return op;
}

void did(optype type, unsigned id) {
	operation *new_op = malloc(sizeof(operation));
	new_op->type = type;
	new_op->id = id;
	stack_append(new_op);
}

operation *undo() {
	if (stack_count < 0) stack_count = 0;
	if (opstack != NULL && stack_count > 0) {
		//printf("undo stack_count: #%u last_type: #%d last_id: id#%u\n", stack_count, opstack[stack_count - 1]->type, opstack[stack_count - 1]->id);
		return opstack[--stack_count];
	}
	else {
		puts("Nothing to undo");
		return create_empty_op();
	}
}

operation *redo() {
	//if (stack_count < 0) stack_count = 0;
	if ( opstack != NULL && stack_count <= stack_size && opstack[stack_count] != NULL) {
		//printf("redo stack_count: #%u last_type: #%d last_id: id#%u\n", stack_count, opstack[stack_count]->type, opstack[stack_count]->id);
		return opstack[stack_count++];
	}
	else {
		puts("Nothing to redo");
		return create_empty_op();
	}
}