#pragma once
#ifndef __UNDOSTACK_H__
#define __UNDOSTACK_H__
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIST_ALLOCATE_STEP 16

typedef enum {OPEREATION_NULL, OPERATION_ADD, OPERATION_DELETE} optype;

typedef struct {
	optype type;
	unsigned id;
} operation;

void did(optype op, unsigned id);
operation *undo();
operation *redo();

#endif